# SISOP Modul 1 (Kelompok E10)

## Anggota Kelompok

1. Eldenabih Tavirazin Lutvie   (5025201213)
2. Hafiz Kurniawan              (5025201032)
3. Michael Ariel Manihuruk      (5025201158)

## Soal 1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.
<br>
a.	Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh
<br>
b.	Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut
<br>
i.	Minimal 8 karakter
<br>
ii.	Memiliki minimal 1 huruf kapital dan 1 huruf kecil
<br>
iii.	Alphanumeric
<br>
iv.	Tidak boleh sama dengan username
<br>
c.	Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
<br>
i.	Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
<br>
ii.	Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
<br>
iii.	Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
<br>
iv.	Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
<br>
d.	Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
<br>
i.	dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
<br>
ii.	att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

## Penyelesaian Soal 1

### Cara Pengerjaan
#### 1a.
Membuat file user.txt pada folder users dengan potongan code berikut.
```bash
if [[ ! -d "./users" ]]
then
	mkdir ./users
    touch ./users/user.txt
fi
```
Dan menginput data user dengan potongan kode berikut.
```bash
echo $username $password>> users/user.txt
```
#### 1b.
Membuat password hidden dengan menggunakan command -s sebagai berikut.
```bash
read  -s password
```
Menaruh syarat password "Minimal 8 karakter" dengan potongan kode berikut.
```bash
elif [[ "$passcharlength" -lt 8 ]]
then 
    echo "Password error: Must contain at least 8 character long"
```
Menaruh syarat password "Memiliki minimal 1 huruf kapital dan 1 huruf kecil" dengan potongan kode berikut.
```bash
elif ! [[ "$password" =~ [[:upper:]] ]] || ! [[ "$password" =~ [[:lower:]] ]]
then
    echo "Password error: Must consist of both uppercase and lowercase character"
```
Menaruh syarat password "Alphanumeric" dengan potongan kode berikut.
```bash
elif [[ "$password" != *[0-9]* ]]
then
	echo "Password error: Must be alphanumeric"
```
Menaruh syarat password "Tidak boleh sama dengan username" dengan potongan kode berikut.
```bash
if [[ $password == $username ]]
then 
    echo "Password error: Can't be the same as username"
```
#### 1c.
Membuat file log.txt pada folder log dengan potongan code berikut.
```bash
if [[ ! -d "./log" ]]
then
	mkdir ./log
    touch ./log/log.txt
fi
```
Kemudian mencatat message pada log.txt dengan potongan kode berikut.
<br>
i.	Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
```bash
if grep -wqF $username "users/user.txt"
then 
    echo "User already exists"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "REGISTER: ERROR User already exists" >> ./log/log.txt
```
ii.	Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
```bash
else
    echo $username $password>> users/user.txt
    echo "INFO User $username registered successfully"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "REGISTER: INFO User $username registered successfully" >> ./log/log.txt
```
iii.	Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
```bash
elif ! grep -wqF $username "users/user.txt"
then 
    echo "User does not exist"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./log/log.txt

else
    echo "Wrong password"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./log/log.txt
```
iv.	Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
```bash
elif grep -wqF "$userpass" "users/user.txt"
then
    echo "INFO User $username logged in"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "LOGIN: INFO User $username logged in" >> ./log/log.txt
```
#### 1d.
Membaca command yang diminta dengan potongan kode berikut.
```bash
echo "Input command:"
    read command count
```
i.
Apabila belum ada folder zip yang diminta, maka akan dibuatkan folder baru seperti potongan kode berikut.
```bash
if [[ ! -f "$(date +%Y-%m-%d)_$username.zip" ]]
    then
	    mkdir $(date +%Y-%m-%d)_$username
        n=0
```
Apabila sudah terdapat folder zip yang diminta, maka akan diunzip seperti potongan kode berikut.
```bash
else
    unzip -P $password $(date +%Y-%m-%d)_$username.zip
    rm $(date +%Y-%m-%d)_$username.zip
    n=$(find $(date +%Y-%m-%d)_$username -type f | wc -l)
```
Untuk setiap dari n (banyak gambar) yang diminta, maka program akan mendownload gambar seperti potongan kode berikut.
```bash
for(( i=$n+1; i<=$count+$n; i++ ))
    do
        if [ $i -lt 10 ]
	    then    
            wget https://loremflickr.com/320/240 -O $(date +%Y-%m-%d)_$username/PIC_0$i.jpg
        else
            wget https://loremflickr.com/320/240 -O $(date +%Y-%m-%d)_$username/PIC_$i.jpg
        fi
    done
```
Kemudian program akan menzip folder dengan password user serta menghapus folder seperti potongan kode berikut.
```bash
zip -P $password -qr $(date +%Y-%m-%d)_$username.zip $(date +%Y-%m-%d)_$username/
rm -rf $(date +%Y-%m-%d)_$username
```
ii.
Menghitung jumlah login berhasil dengan login gagal dengan potongan kode berikut.
```bash
if [[ $command == att ]]
    then
        successlogincount="$(grep -ow "LOGIN: INFO User $username logged in" ./log/log.txt | wc -l)"
        faillogincount="$(grep -ow "LOGIN: ERROR Failed login attempt on user $username" ./log/log.txt | wc -l)"
        totallogincount=$((faillogincount + successlogincount))
        echo "Total Login attempt = $totallogincount with $successlogincount Success login attempt and $faillogincount Failed login attempt"
```
### Screenshoot hasil
Register berhasil apabila terminal muncul seperti gambar berikut.<br>
![image](/uploads/4c238bcfaf542f56d56b7e464b12cb4e/image.png) <br>
Isi dari user.txt ditampilkan dengan gambar berikut.<br>
![image](/uploads/9763fecd7429a6d8bc2bb03b27c95cef/image.png)<br>
Command "dl N" dijalankan seperti gambar berikut.<br>
![image](/uploads/025037fa869b36a8bfb5536c6793d000/image.png)<br>
Isi dari zip ditampilkan dengan gambar berikut.<br>
![image](/uploads/78b385944dcf63701f07b4d7f3c6fa60/image.png)<br>
Apabila user gagal untuk login maka akan muncul tampilan berikut.<br>
![image](/uploads/a1ee4d3997a4299eca44a9b1c1975111/image.png)<br>
Apabila user berhasil login dan meminta command "att" maka akan muncul tampilan berikut.<br>
![image](/uploads/a2208777f6247dc2f48128c04b4db4f6/image.png)<br>
Isi dari log.txt ditampilkan dengan gambar berikut.<br>
![image](/uploads/8e3b1c7ad0fe8bfb034e652314c8768e/image.png)<br>

### Kendala
Masih bingung dengan beberapa command seperti -s, -n, -q, dll serta cara untuk memaksimalkan penggunaanya.

## Soal 2
#### Progam
___
Pada studi kasus 2, kita akan mengidentifikasi log website daffa.info dimana file log directory log_website_daffainfo.log disimpan dalam variabel ``logLocation``. Serta hasil identifikasinya disimpan pada folder directory forensic_log_website_daffainfo_log disimpan dalam variabel ``folderLocation``.

Mulanya, membuat dahulu folder directory ``folderLocation`` untuk menyimpan hasil identifikasi. 
Kita akan mengecek apakah folder directorynya sudah ada, apabila sudah ada maka akan hapus directory tersebut dengan ``rm -rf $folderLocation``. Kemudian kita buat directory baru dengan ``mkdir $folderLocation``. Apabila belum ada directorynya maka akan membuat directory baru dengan ``mkdir $folderLocation``. Serta shebang ``!# /bin/bash`` jangan dilupakan.

```bash
#! /bin/bash

logLocation=/home/$USERNAME/log_website_daffainfo.log
folderLocation=/home/$USERNAME/forensic_log_website_daffainfo_log

if [[ -d "$folderLocation" ]]
then
	rm -rf $folderLocation
	mkdir $folderLocation
else
	mkdir $folderLocation
fi
```

Pada bagian ini, kita akan mencari berapa banyak rata-rata ip request per jamnya. kita akan identifikasi file log dengan ``cat $logLocation``. Kemudian ekstraksi file lognya dengan menggunakan ``awk`` dengan mencari pattern pada log dengan menggunakan ``gsub(/"/,"",$3)`` kemudian kita simpan hasilnya pada array ``collection``. Untuk mendapatkan hasil rata rata request per jam ``avg`` dengan membagi total ip request ``sum`` dengan banyak jam  ``count``.
Kita sudah mendapatkan ``avg`` lalu kita tampilkan dengan ``print`` dan hasilnya kita simpan pada file ``ratarata.txt`` didalam directory ``forensic_log_website_daffainfo_log`` dalam ``folderLocation``. Menyimpannya dapat dengan ``>> $folderLocation/ratarata.txt``.
```bash
cat $logLocation | awk -F: '{gsub(/"/,"",$3) collection[$3]++}
	END{
		for(i in collection ){
			count++
			sum+=collection[i]
		}
		avg=sum/count
		printf "Rata-rata serangan adalah " avg " requests per jam\n"
	}' >> $folderLocation/ratarata.txt
```

Pada bagian ini kita mencari ip paling banyak mengakses dan banyaknya akses ip tersebut. Kita akan identifikasi file log dengan ``cat $logLocation``. Kemudian kita ekstraksi log dengan menggunakan `awk` dengan mencari pattern pada log dengan menggunakan ``gsub(/"/, "", $1)`` kemudian ip yang didapatkan disimpan pada array ``ip``. Selanjutnya kita akan mencari ip paling banyak mengakses dengan membandingkan ``iptarget`` sebagai ip yang mengakses serta ``max`` sebagai banyaknya ip tersebut mengakses. Kita akan cetak output dengan ``print``. Hasilnya kita simpan pada file ``result.txt`` pada ``$folderlocation``.
```bash
cat $logLocation | awk -F: '{gsub(/"/, "", $1) ip[$1]++}
	END{
		max=0
		iptarget
		for( i in ip ){
			if(max < ip[i]){
				iptarget=i
				max=ip[iptarget]
			}
		}
		print "IP yang paling banyak mengakses server adalah: " iptarget " sebanyak " max " requests\n"
	}' >> $folderLocation/result.txt
```
Pada bagian ini kita mencari banyaknya ip request yang menggunakan curl user agent. Kita identifikasi file log dengan ``cat $logLocation``. Kita akan mencari log yang memuat string pattern ``curl`` .     Kemudian kita hitung dan disimpan pada variabel ``curlcount``. Output kita gunakan ``print`` dimana hasil tersebut disimpan dalam file ``result.txt`` didalam directory ``$folderLocation``.
```bash
cat $logLocation | awk '/curl/ {++curlcount}
	END{ 
		print "Ada " curlcount " requests yang menggunakan curl sebagai user-agent\n"
	}' >> $folderLocation/result.txt
```
Pada bagian ini kita akan mencari ip yang mengakses pada jam 02.00 - 02.59 pada tanggal 22 jan 2022. kita identifikasi log dengan ``cat $logLocation``. Kemudian kita akan cari string pattern yang memuat string 22 Jan 2022 dan jam 02.00 - 02.59 dengan ``/22/ && /Jan/ && /2022:02/``. kita simpan ip yang berada pada kolom ``$1`` pada array ``ip``. Kemudian kita cetak hasilnya dengan looping ``for`` pada array ``ip`` serta kita simpan pada file ``result.txt`` dalam directory
``forensic_log_website_daffainfo_log`` didalam ``$folderLocation``.

```bash
cat $logLocation | awk -F: ' /22/ && /Jan/ && /2022:02/ {gsub(/"/, "", $1)  ip[$1]++ }
	END{	
		for (i in ip){
			print i
		}
	}' >> $folderLocation/result.txt
```
## Output
___
Tampilan folder forensic_log_web_website_daffainfo_log yang dibuat.
![folder forensic_log_website_daffainfo_log]
![201](/uploads/e1382c8bdb6cff15cea8ccd1b8f316cc/201.png)

Tampilan isi folder forensic_log_website_daffainfo_log yang memuat file ``ratarata.txt`` dan file ``result.txt``.
![Tampilan isi folder forensic_log_website_daffainfo_log]
![202](/uploads/546353f4c3f0aab3183426d7697ac3f2/202.png)

Tampilan hasil file ``ratarata.txt``.
![Tampilan hasil file ratarata.txt]
![203](/uploads/b58c507917fab80b38e29d09211a4dd4/203.png)

Tampilan hasil file ``result.txt``.
![Tampilan file result.txt]
![204](/uploads/8f4784ffa08d7c76512c5762eb49246e/204.png)

## Kendala Pengerjaan
___
Kendala yang dialami ketika mengerjakan studi kasus 2 ini adalah langkah pengerjaan yang telah disusun tidak selalu membuahkan hasil yang selaras. Sehingga harus beradaptasi dengan mengubah langkah yang telah disusun.
___

## Soal 3
Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command *free -m. Untuk disk gunakan command *du -sh <target_path>. Catat semua metrics yang didapatkan dari hasil *free -m. Untuk hasil *du -sh <target_path> catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah */home/{user}/*.

<br>
a.	Masukkan semua metrics ke dalam suatu file log bernama *metrics_{YmdHms}.log. {YmdHms}* adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
<br>
b.	Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
<br>
c.	Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format *metrics_agg_{YmdH}*.log
<br>
d.	Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.
<br>


## A. Metrics log

File minute_log.sh akan mencatat *mem_total, **mem_used, **mem_free, **mem_shared, **mem_buff, **mem_available, **swap_total, **swap_used, **swap_free, **path, **path_size* setiap dijalankan pada sebuah file txt yang akan diteruskan ke sebuah file .log dengan format penamaan *metrics_{YmdHms}*.log. Pada tahap ini pengecekan memory yang kosong menggunakan command *free -m, dan size menggunakan du -sh.

## B. Pencatatan per menit

Untuk menjalankan script secara otomatis menggunakan crontab. Script minute_log.sh dijalankan tiap menit dan aggregate_minutes_to_hourly_log.sh dijalankan tiap jam.

## C. Script agregasi

- Redirect metrics ke file log metrics_$dtFull$dtTime.log

Metrics memory yang sudah didapatkan dari Langkah sebelumnya diredirect ke file log metrics_$dtFull$dtTime.log menggunakan AWK untuk setiap elemen yang didapatkan.
Selanjutnya untuk size directory menggunakan du -sh.

- Redirect file log metrics_$dtFull$dtTime.log ke file log metrics_agg_$dtFull$dtHour.log

Data metrics pada file log metrics_$dtFull$dtTime.log diredirect ke file log metrics_agg_$dtFull$dtHour.log dalam bentuk file log metrics_agg_$dtFull$dtHour.log dan file log metrics_$dtFull$dtTime.log serta diberikan chmod 400 untuk pembatasan akses berupa read kepada user.

- aggregate metrics_agg_$dtDate$dtHour.log

File ini akan mencatat metrics minimum, maximum, dan rata-rata setiap jam menggunakan 3 fungsi dengan kegunaan masing-masing. 3 fungsi tersebut adalah *min_func(), **max_func(), dan **avg_func(). Fungsi ini memiliki cara kerja yang sangat mirip yakni menggunakan awk untuk mengakses data metrics pada file temp metrics_agg_$dtDate$dtHour.log menentukan nilai minimum, maximum, dan rata-ratanya dengan membandingkan data metrics pada jam itu. Selanjutnya hasil perbandingan akan diredirect ke file log metrics_agg_$dtDate$dtHour.log serta diberikan chmod 400 untuk pembatasan akses berupa read kepada user..

## Output

1. Contoh list file metrics_{YmdHms}.log dan metrics_agg_{YmdH}.log setelah tiga jam dijalankan:

![1](soal3/img/list.jpeg)
 
2. Contoh file metrics_{YmdHms}.log setelah beberapa kali dijalankan:

![2](soal3/img/minute.jpg)

3. Contoh file metrics_agg_{YmdH}.log setelah beberapa kali dijalankan:

![3](soal3/img/aggregate.jpg)


## Kendala

- Mencoba menjalankan menggunakan crontab 

- Memahami beberapa sintaks dalam AWK

- Mencari algoritma dalam melakukan agregasi
