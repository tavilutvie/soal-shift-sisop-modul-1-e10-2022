#! /bin/bash

logLocation=/home/$USERNAME/log_website_daffainfo.log
folderLocation=/home/$USERNAME/forensic_log_website_daffainfo_log

if [[ -d "$folderLocation" ]]
then
	rm -rf $folderLocation
	mkdir $folderLocation
else
	mkdir $folderLocation
fi

cat $logLocation | awk -F: '{gsub(/"/,"",$3) collection[$3]++}
	END{
		for(i in collection ){
			count++
			sum+=collection[i]
		}
		avg=sum/count
		printf "Rata-rata serangan adalah " avg " requests per jam\n"
	}' >> $folderLocation/ratarata.txt

cat $logLocation | awk -F: '{gsub(/"/, "", $1) ip[$1]++}
	END{
		max=0
		iptarget
		for( i in ip ){
			if(max < ip[i]){
				iptarget=i
				max=ip[iptarget]
			}
		}
		print "IP yang paling banyak mengakses server adalah: " iptarget " sebanyak " max " requests\n"
	}' >> $folderLocation/result.txt


cat $logLocation | awk '/curl/ {++curlcount}
	END{ 
		print "Ada " curlcount " requests yang menggunakan curl sebagai user-agent\n"
	}' >> $folderLocation/result.txt

cat $logLocation | awk -F: ' /22/ && /Jan/ && /2022:02/ {gsub(/"/, "", $1)  ip[$1]++ }
	END{	
		for (i in ip){
			print i
		}
	}' >> $folderLocation/result.txt
