#!/bin/bash

echo "Enter Username:"
read username
echo "Enter Password:"
read  -s password

passcharlength=$(echo -n "$password" | wc -c)
userpass="${username} ${password}"

if [[ $password == $username ]]
then 
    echo "Password error: Can't be the same as username"

elif [[ "$passcharlength" -lt 8 ]]
then 
    echo "Password error: Must contain at least 8 character long"

elif [[ "$password" != *[0-9]* ]]
then
	echo "Password error: Must be alphanumeric"

    
elif ! [[ "$password" =~ [[:upper:]] ]] || ! [[ "$password" =~ [[:lower:]] ]]
then
    echo "Password error: Must consist of both uppercase and lowercase character"

elif grep -wqF "$userpass" "users/user.txt"
then
    echo "INFO User $username logged in"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "LOGIN: INFO User $username logged in" >> ./log/log.txt

    echo "Input command:"
    read command count

    if [[ $command == att ]]
    then
        successlogincount="$(grep -ow "LOGIN: INFO User $username logged in" ./log/log.txt | wc -l)"
        faillogincount="$(grep -ow "LOGIN: ERROR Failed login attempt on user $username" ./log/log.txt | wc -l)"
        totallogincount=$((faillogincount + successlogincount))
        echo "Total Login attempt = $totallogincount with $successlogincount Success login attempt and $faillogincount Failed login attempt"
    
    elif [[ $command == dl ]] && [[ -n "$count" ]]
    then
	    if [[ ! -f "$(date +%Y-%m-%d)_$username.zip" ]]
	    then
		    mkdir $(date +%Y-%m-%d)_$username
		    n=0
            
	    else
		    unzip -P $password $(date +%Y-%m-%d)_$username.zip
	        rm $(date +%Y-%m-%d)_$username.zip
	        n=$(find $(date +%Y-%m-%d)_$username -type f | wc -l)
	    fi

        for(( i=$n+1; i<=$count+$n; i++ ))
	    do
            if [ $i -lt 10 ]
		    then    
                wget https://loremflickr.com/320/240 -O $(date +%Y-%m-%d)_$username/PIC_0$i.jpg
            else
                wget https://loremflickr.com/320/240 -O $(date +%Y-%m-%d)_$username/PIC_$i.jpg
            fi
	    done

	    zip -P $password -qr $(date +%Y-%m-%d)_$username.zip $(date +%Y-%m-%d)_$username/
	    rm -rf $(date +%Y-%m-%d)_$username

    else
	    echo "Unknown command, please try again!"
    fi

elif ! grep -wqF $username "users/user.txt"
then 
    echo "User does not exist"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./log/log.txt

else
    echo "Wrong password"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "LOGIN: ERROR Failed login attempt on user $username" >> ./log/log.txt

fi



