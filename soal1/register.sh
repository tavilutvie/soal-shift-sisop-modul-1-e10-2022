#!/bin/bash
#PROMPTS

echo "Enter Username:"
read username
echo "Enter Password:"
read  -s password

if [[ ! -d "./log" ]]
then
	mkdir ./log
    touch ./log/log.txt
fi

if [[ ! -d "./users" ]]
then
	mkdir ./users
    touch ./users/user.txt
fi


passcharlength=$(echo -n "$password" | wc -c)

if grep -wqF $username "users/user.txt"
then 
    echo "User already exists"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "REGISTER: ERROR User already exists" >> ./log/log.txt

elif [[ $password == $username ]]
then 
    echo "Password error: Can't be the same as username"

elif [[ "$passcharlength" -lt 8 ]]
then 
    echo "Password error: Must contain at least 8 character long"

elif [[ "$password" != *[0-9]* ]]
then
	echo "Password error: Must be alphanumeric"

    
elif ! [[ "$password" =~ [[:upper:]] ]] || ! [[ "$password" =~ [[:lower:]] ]]
then
    echo "Password error: Must consist of both uppercase and lowercase character"

else
    echo $username $password>> users/user.txt
    echo "INFO User $username registered successfully"
    echo -n "$(date +'%m/%d/%Y %H:%M:%S ')" >> ./log/log.txt
    echo "REGISTER: INFO User $username registered successfully" >> ./log/log.txt
fi


